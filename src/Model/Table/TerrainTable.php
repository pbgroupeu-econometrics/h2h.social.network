<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Terrain Model
 *
 * @method \App\Model\Entity\Terrain newEmptyEntity()
 * @method \App\Model\Entity\Terrain newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Terrain[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Terrain get($primaryKey, $options = [])
 * @method \App\Model\Entity\Terrain findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Terrain patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Terrain[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Terrain|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Terrain saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Terrain[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Terrain[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Terrain[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Terrain[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class TerrainTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('terrain');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('map')
            ->requirePresence('map', 'create')
            ->notEmptyString('map');

        $validator
            ->requirePresence('matrix', 'create')
            ->notEmptyString('matrix');

        return $validator;
    }
}

<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Bot Model
 *
 * @method \App\Model\Entity\Bot newEmptyEntity()
 * @method \App\Model\Entity\Bot newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Bot[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Bot get($primaryKey, $options = [])
 * @method \App\Model\Entity\Bot findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Bot patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Bot[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Bot|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Bot saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Bot[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Bot[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Bot[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Bot[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class BotTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('bot');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('user')
            ->requirePresence('user', 'create')
            ->notEmptyString('user');

        $validator
            ->requirePresence('level', 'create')
            ->notEmptyString('level');

        return $validator;
    }
}

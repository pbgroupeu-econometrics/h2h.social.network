<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MapTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MapTable Test Case
 */
class MapTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\MapTable
     */
    protected $Map;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Map',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Map') ? [] : ['className' => MapTable::class];
        $this->Map = $this->getTableLocator()->get('Map', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Map);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

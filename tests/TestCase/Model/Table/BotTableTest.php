<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BotTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BotTable Test Case
 */
class BotTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\BotTable
     */
    protected $Bot;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Bot',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Bot') ? [] : ['className' => BotTable::class];
        $this->Bot = $this->getTableLocator()->get('Bot', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Bot);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

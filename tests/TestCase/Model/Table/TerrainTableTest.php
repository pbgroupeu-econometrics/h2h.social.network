<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TerrainTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TerrainTable Test Case
 */
class TerrainTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\TerrainTable
     */
    protected $Terrain;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Terrain',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Terrain') ? [] : ['className' => TerrainTable::class];
        $this->Terrain = $this->getTableLocator()->get('Terrain', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Terrain);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
